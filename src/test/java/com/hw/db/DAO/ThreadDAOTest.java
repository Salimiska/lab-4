package com.hw.db.DAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

public class ThreadDAOTest {
    private static JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        new ThreadDAO(jdbcTemplate);
    }

    @Test
    public void treeSortSinceNullAndDescNullAndLimitNullTest() {
        String SQL = "SELECT * FROM \"posts\" WHERE thread = ? " + " ORDER BY branch" + ";";
        ThreadDAO.treeSort(5, null, null, null);
        Mockito.verify(jdbcTemplate).query(eq(SQL), (RowMapper<Object>) any(), eq(5));
    }

    @Test
    public void treeSortSinceNotNullAndDescNullAndLimitNullTest() {
        String SQL = "SELECT * FROM \"posts\" WHERE thread = ? " +
                " AND branch > (SELECT branch " +
                " FROM posts WHERE id = ?) " +
                " ORDER BY branch" + ";";
        ThreadDAO.treeSort(5, null, 8, null);
        Mockito.verify(jdbcTemplate).query(eq(SQL), (RowMapper<Object>) any(), eq(5), eq(8));
    }

    @Test
    public void treeSortSinceNotNullAndDescAndLimitNotNullTest() {
        String SQL = "SELECT * FROM \"posts\" WHERE thread = ? " +
                " AND branch < (SELECT branch " +
                " FROM posts WHERE id = ?) " +
                " ORDER BY branch" +
                " DESC " +
                " LIMIT ? " +
                ";";
        ThreadDAO.treeSort(5, 3, 8, true);
        Mockito.verify(jdbcTemplate).query(eq(SQL), (RowMapper<Object>) any(), eq(5), eq(8), eq(3));
    }
}
