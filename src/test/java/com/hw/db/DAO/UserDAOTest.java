package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

public class UserDAOTest {
    private static JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        new UserDAO(jdbcTemplate);
    }

    @Test
    public void changeAllNullTest() {
        User user = new User();
        UserDAO.Change(user);
        Mockito.verify(jdbcTemplate, Mockito.never()).update(anyString(), (Object) any());
    }

    @Test
    public void changeAllSetTest() {
        String SQL = "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;";
        User user = new User("some nick", "some email", "some name", "some about");
        UserDAO.Change(user);
        Mockito.verify(jdbcTemplate).update(SQL, user.getEmail(), user.getFullname(), user.getAbout(), user.getNickname());
    }

    @Test
    public void changeEmailNullAndOtherSetTest() {
        String SQL = "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;";
        User user = new User("some nick", null, "some name", "some about");
        UserDAO.Change(user);
        Mockito.verify(jdbcTemplate).update(SQL, user.getFullname(), user.getAbout(), user.getNickname());
    }

    @Test
    public void changeEmailNullAndFullnameNullAndOtherSetTest() {
        String SQL = "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;";
        User user = new User("some nick", null, null, "some about");
        UserDAO.Change(user);
        Mockito.verify(jdbcTemplate).update(SQL, user.getAbout(), user.getNickname());
    }
}

