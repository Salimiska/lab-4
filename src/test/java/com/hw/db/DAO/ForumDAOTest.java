package com.hw.db.DAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

public class ForumDAOTest {
    private static JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(jdbcTemplate);
    }

    @Test
    public void userListSinceNullAndDescNullAndLimitNullTest() {
        String SQL = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;";
        List<Object> conditions = List.of("some slug");
        ForumDAO.UserList("some slug", null, null, null);
        Mockito.verify(jdbcTemplate).query(eq(SQL), eq(conditions.toArray()), (RowMapper<Object>) any());
    }

    @Test
    public void userListSinceNotNullAndDescNullAndLimitNullTest() {
        String SQL = "SELECT nickname,fullname,email,about FROM forum_users" +
                " WHERE forum = (?)::citext" + " AND  nickname > (?)::citext" + " ORDER BY nickname" + ";";
        List<Object> conditions = List.of("some slug", "some since");
        ForumDAO.UserList("some slug", null, "some since", null);
        Mockito.verify(jdbcTemplate).query(eq(SQL), eq(conditions.toArray()), (RowMapper<Object>) any());

    }

    @Test
    public void userListSinceNotNullAndDescAndLimitNotNullTest() {
        String SQL = "SELECT nickname,fullname,email,about FROM forum_users" +
                " WHERE forum = (?)::citext" +
                " AND  nickname < (?)::citext" +
                " ORDER BY nickname" +
                " desc" +
                " LIMIT ?" +
                ";";
        List<Object> conditions = List.of("some slug", "some since", 5);
        ForumDAO.UserList("some slug", 5, "some since", true);
        Mockito.verify(jdbcTemplate).query(eq(SQL), eq(conditions.toArray()), (RowMapper<Object>) any());
    }


    @Test
    public void userListSinceNotNullAndNotDescAndLimitNotNullTest() {
        String SQL = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";
        List<Object> conditions = List.of("some slug", "some since", 5);
        ForumDAO.UserList("some slug", 5, "some since", false);
        Mockito.verify(jdbcTemplate).query(eq(SQL), eq(conditions.toArray()), (RowMapper<Object>) any());
    }
}
