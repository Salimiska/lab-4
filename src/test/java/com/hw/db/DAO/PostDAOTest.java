package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Timestamp;

import static org.mockito.ArgumentMatchers.*;

public class PostDAOTest {
    private static JdbcTemplate jdbcTemplate;
    private static Timestamp now = new Timestamp(System.currentTimeMillis());
    private static Timestamp notNow = new Timestamp(System.currentTimeMillis() - 36000);

    @Before
    public void setUp() {
        jdbcTemplate = Mockito.mock(JdbcTemplate.class);
        new PostDAO(jdbcTemplate);
        Post toch = new Post("other author", notNow, "other from", "other msg", 4, 2, true);
        Mockito.when(jdbcTemplate.queryForObject(
                eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), (RowMapper<Object>) any(), eq(5))
        ).thenReturn(toch);
    }

    @Test
    public void setPostAllNullTest() {
        Post post = new Post();
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate, Mockito.never()).update(anyString(), (RowMapper<Object>) any());
    }

    @Test
    public void setPostAllSetTest() {
        Post post = new Post("some author", now, "some from", "some msg", 4, 2, true);
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "some author",
                "some msg",
                now,
                5);
    }

    @Test
    public void setPostAuthorNotNullTest() {
        Post post = new Post();
        post.setAuthor("some author");
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;",
                "some author",
                5);
    }

    @Test
    public void setPostMessageNotNullTest() {
        Post post = new Post();
        post.setMessage("some message");
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;",
                "some message",
                5);
    }

    @Test
    public void setPostCreatedNotNullTest() {
        Post post = new Post();
        post.setCreated(now);
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                now,
                5);
    }

    @Test
    public void setPostAuthorNotNullAndMessageNotNullTest() {
        Post post = new Post();
        post.setAuthor("some author");
        post.setMessage("some message");
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;",
                "some author",
                "some message",
                5);
    }

    @Test
    public void setPostAuthorNotNullAndCreatedNotNullTest() {
        Post post = new Post();
        post.setAuthor("some author");
        post.setCreated(now);
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "some author",
                now,
                5);
    }

    @Test
    public void setPostMessageNotNullAndCreatedNotNullTest() {
        Post post = new Post();
        post.setMessage("some message");
        post.setCreated(now);
        PostDAO.setPost(5, post);
        Mockito.verify(jdbcTemplate).update("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "some message",
                now,
                5);
    }
}
